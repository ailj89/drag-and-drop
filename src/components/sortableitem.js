import React, { Component } from 'react';
import Sortable from 'react-anything-sortable';
import ImageItem from './ImageItem';

class SortableItem extends Component{
	constructor() {
    super();
    this.state = {};
  }

  handleSort(data) {
    this.setState({
      result: data.join(' ')
    });
	}
	
	handleDragStart(e) {
		this.style.opacity = '0.4';  // this / e.target is the source node.
	}
	
	

	render(){
		return(
			<section className="main">
				<Sortable onSort={this.handleSort}>
					<div className="point one">
						<ImageItem src="https://via.placeholder.com/350x150" sortData="desktop" key={1} />

						<h2>Introduction to Habitat</h2>

						<p>Sitecore Habitat is a rang of sites demonstrating the capabilities of the SItecore Experience Platform.</p>
						<p> The solution is built on the Sitecore Helix guidelines, which focuses on increasing productivity and quality in Sitecore projects.</p>

						<span className="read-more">Read more</span>
					</div>

					<div className="point two">
						<ImageItem src="https://via.placeholder.com/300x150" sortData="tablet" key={2} />

						<h2>Modules</h2>

						<p>The Habitat example implementation has a range of modules which covers the basics of a Sitecore website.</p>

						<span className="read-more">Read more</span>
					</div>

					<div className="point three">
						<ImageItem src="https://via.placeholder.com/325x150" sortData="creditcard" key={3} />

						<h2>Getting Started</h2>

						<p>Sitecore Helix a defined methodology with convetions and practices &emdash; Habitat is an example implementation available for your understanding.</p>
						<p> The solution is built on the Sitecore Helix guidelines, which focuses on increasing productivity and quality in Sitecore projects.</p>

						<span className="read-more">Read more</span>
					</div>
				</Sortable>
			</section>			
		);
	}
};

export default SortableItem;