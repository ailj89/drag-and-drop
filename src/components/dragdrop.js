import React, { Component } from 'react';

class DragDrop extends Component{

	constructor (props) {
    super(props)
		this.selectedImagesrc = null;
		this.hoveredImageSrc = null;
		this.newSelectedImageSrc = null;
	}
	
	//grabs the source of the dragged image
	handleDragStart = (e) => {
		this.selectedImageSrc = e.target.src;
		// console.log('Current Source is ' + this.selectedImageSrc);
		return this.selectedImage;
	}

	// grabs the source of the imaged currently hovered
	handleDragOver = (e) => {
		e.preventDefault();
		this.hoveredImageSrc = e.target.src;
		// console.log('Hovered Source is ' + this.hoveredImageSrc);
		return this.hoveredImageSrc;
	}

	// replaces the dragged image's source with the source of the hovered image
	handleDragEnd = (e) =>{ 
		this.newSelectedImageSrc = this.hoveredImageSrc;
		e.target.src = this.newSelectedImageSrc;
		// console.log('New Source is ' + this.newSelectedImageSrc);
		return this.newSelectedImageSrc;
	}

	// replaces the image's source with the source of the dragged image
	handleDrop = (e) => {
		e.preventDefault();
		e.target.src = this.selectedImageSrc;
	}
	
	render(){
		return(
			<section className="main">
					<div className="point one">
						<img src="images/pc.png" alt="desktop" 
						draggable="true" 
						onDragStart={this.handleDragStart}
						onDragEnd={this.handleDragEnd}
						onDragOver={this.handleDragOver}
						onDrop={this.handleDrop}
						/>

						<h2>Introduction to Habitat</h2>

						<p>Sitecore Habitat is a rang of sites demonstrating the capabilities of the SItecore Experience Platform.</p>
						<p> The solution is built on the Sitecore Helix guidelines, which focuses on increasing productivity and quality in Sitecore projects.</p>

						<span className="read-more">Read more</span>
					</div>

					<div className="point two">
						<img src="images/tablet.png" alt="tablet" 
						draggable="true" 
						onDragStart={this.handleDragStart}
						onDragEnd={this.handleDragEnd}
						onDragOver={this.handleDragOver}
						onDrop={this.handleDrop} />

						<h2>Modules</h2>

						<p>The Habitat example implementation has a range of modules which covers the basics of a Sitecore website.</p>

						<span className="read-more">Read more</span>
					</div>

					<div className="point three">
						<img src="images/creditcard.png" alt="credit card" 
						draggable="true" 
						onDragStart={this.handleDragStart}
						onDragEnd={this.handleDragEnd}
						onDragOver={this.handleDragOver}
						onDrop={this.handleDrop} />

						<h2>Getting Started</h2>

						<p>Sitecore Helix a defined methodology with convetions and practices &emdash; Habitat is an example implementation available for your understanding.</p>
						<p>The solution is built on the Sitecore Helix guidelines, which focuses on increasing productivity and quality in Sitecore projects.</p>

						<span className="read-more">Read more</span>
					</div>
			</section>			
		);
	}
};

export default DragDrop;