import React, { Component } from 'react';

class Home extends Component{
	render(){
		return(
			<div>
				<section className="carousel">
					<img src="images/carousel.png" alt="Carousel placeholder" />
				</section>

				{this.props.children}

				<section className="download">
					<div className="github">
						<h2>Download Habitat Now!</h2>
						<p>Habitat is available as a Sitecore Package and the sourcecode is available on Github.</p>
						<span className="visit">Visit Habitat on Github</span>
					</div>

					<div className="testimony">
						<img src="images/anders.png" alt="Anders Laub Christoffersen, Sitecore MVP, Pentia A/S" />
						<cite>
							<p>Anders Laub Christoffersen</p>
							<p>Sitecore MVP, Pentia A/S</p>
						</cite>
					
						<p>[Habitat...] is nothing less than groundbreaking, it is a real revolution in the way that Sitecore teaches developers to work their product.</p>
					</div>

					<div className="news">
						<h3>News</h3>
						<span className="read-more">Read more</span>
					</div>

				</section>
			</div>
		)
	}
}

export default Home;