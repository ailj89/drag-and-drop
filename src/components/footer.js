import React, { Component } from 'react';

class Footer extends Component{
	render(){
		return(
			<footer>
				<div className="back-links">
					<ul>
						<li>Community</li>
						<li>Documentation</li>
						<li>Knowledge Base</li>
						<li>Partner Network</li>
						<li>Best Practice Blogs</li>
						<li>Business Blogs</li>
						<li>Technical Blogs</li>
						<li>Contact Sitecore</li>
					</ul>
				</div>

				<div className="about">
					<h3>About Habitat</h3>
					<p>Habitat sites are demonstration sites for the sitecore&reg; Experience Platform&trade;. The sites demonstrate the full set of capabilities and potential of the platform through a number of both techincal and business scenarios.</p>
					<span className="read-more">Example available on GitHub</span>
				</div>

				<div className="contact">
					<h3>Contact information</h3>
					<ul>
						<li>Sitecore Corporation</li>
						<li>2320 Marinship Way</li>
						<li>Sausalita, CA 94965</li>
						<li>USA</li>
					</ul>

					<ul>
						<li>+1 415 38 0600</li>
						<li>sales@sitecore.net</li>
					</ul>
				</div>

				<div className="context">
					<p>&copy; Copyright 2017, Sitecore. All Rights Reserved</p>
					<p>"Habitat" as used here is for demonstration purposes only and to represent the software and services provided by Sitecore further described at www.sitecore.net.</p>
				</div>
			</footer>
		)
	}
}

export default Footer;