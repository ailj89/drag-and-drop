import React, { Component } from 'react';

class Nav extends Component{
	render(){
		return(
			<header>
				<div>
				<i className="fas fa-bars fa-3x"></i>
					<ul className="social">
						<li><i className="fab fa-facebook-f"></i></li>
						<li><i className="fab fa-twitter"></i></li>
						<li><i className="fab fa-youtube"></i></li>
					</ul>
					<img src="images/logo.png" alt="Habitat" />
				</div>
				
				<nav>
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">About Habitat</a></li>
						<li><a href="#">Modules</a></li>
						<li><a href="#">More Info</a></li>
					</ul>
				</nav>

			<div className="products">
				<ul className="sub-nav">
					<li><a href="#">Habitat</a></li>
					<li><a href="#">Helix</a></li>
					<li><a href="#">Sitecore.net</a></li>
				</ul>

				<ul>
					<li><i className="fas fa-globe-americas"></i></li>
					<li><i className="fas fa-user"></i></li>
					<li><i className="fas fa-search"></i></li>
				</ul>
			</div>
			</header>
		)
	}
}

export default Nav;