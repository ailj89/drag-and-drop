import React, { Component } from 'react';

class DragDrop extends Component{

	state = {
		tasks:[
			{ name: "Learn Angular",
				category: "wip",
				bgcolor: "yellow"},
			{ name: "React",
				category: "wip",
				bgcolor: "pink"},
			{ name: "Vue",
				category: "complete",
				bgcolor: "skyblue"
			}
		]
	}

		//event handler
		onDragStart = (e, id) => {
			console.log('dragstart:', id);
			e.dataTransfer.setData("id", id);
		}

		onDragOver = (e) => {
			e.preventDefault();
		}

		onDrop = (e, cat) => {
			let id = e.dataTransfer.getData("id");

			let tasks = this.state.tasks.filter((task) =>{
				if (task.name == id){
					task.category = cat;
				}
				return task;
			});

			this.setState({
				...this.state,
				tasks
			});
		}

	render(){

		//data and categories
		var tasks = {
			wip: [],
			complete: []
		}

		// making item draggable

		this.state.tasks.forEach((task) =>{
			tasks[task.category].push(<div key={task.name} 
				onDragStart={ (e) => this.onDragStart(e, task.name)}
				draggable
				className="draggable"
				style={{backgroundColor: task.bgcolor}}>
					{task.name}
				</div>);
		});

		//creating droppable container

		return(
			<div className="container-drag">
				<h2 className="header">Drag &amp; Drop Demo</h2>
				<div className="wip"
						onDragOver={(e) => this.onDragOver(e)}
						onDrop={(e) => this.onDrop(e, "wip")}>
					<span className="task-header">WIP</span>
					{tasks.wip}
				</div>
				<div className="droppable" 
					onDragOver={ (e) => this.onDragOver}
					onDrop={ (e) => this.onDrop(e, "complete")}
					>
					<span className="task-header">Complete</span>
					{tasks.complete}
				</div>
			</div>
		)
	}

}

export default DragDrop;