import React, { Component } from 'react';
import './App.css';
import Home from './components/home';
import Nav from './components/nav';
import Footer from './components/footer';
import DragDrop from './components/dragdrop';

class App extends Component {
  render() {
    return (
      <div className="App">
				<Nav />
				<Home>
					<DragDrop />
				</Home>
				<Footer />
      </div>
    );
  }
}

export default App;